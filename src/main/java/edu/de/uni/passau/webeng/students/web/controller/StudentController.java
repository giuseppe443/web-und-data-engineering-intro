package edu.de.uni.passau.webeng.students.web.controller;

import edu.de.uni.passau.webeng.students.application.service.StudentService;

import edu.de.uni.passau.webeng.students.web.dto.CourseDto;
import edu.de.uni.passau.webeng.students.web.dto.StudentDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class StudentController {

    private final StudentService studentService;
    private static final String template = "Hello, %s!";
    private ModelMapper modelMapper;
    private String test;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    // Todo Implement controller methods.
    // The Rest API mapping has to be done here:
    @GetMapping("/randomText")
    public String retrieveStudents() {
        return "Student X is dead";
    }

    @GetMapping(value = "/students/{id}")
    public @ResponseBody
    ResponseEntity<StudentDto> getOrder(@PathVariable("id") String id){
        StudentDto studentDto = studentService.getStudentDtoById(id);
        return new ResponseEntity<>(studentDto, HttpStatus.OK);
    }

    @GetMapping(value = "/students/{id}/courses")
    public @ResponseBody
    ResponseEntity<List<CourseDto>> getCourses(@PathVariable("id") String id){
        List<CourseDto> courseDtos = studentService.getCoursesOfStudent(studentService.getStudentById(id));
        return new ResponseEntity<>(courseDtos, HttpStatus.OK);
    }

    @GetMapping(value = "/students/{id}/courses/{cid}")
    public void addStudentoCourse(@PathVariable("id") String id, @PathVariable("cid") String cid){

        //TODO Maybe delete the c of cid to make traversing easier.
        //TODO strudenService add Course method.
        studentService.addCourseToStudent(id,cid);
    }


    // Todo Implement controller methods.
    // The Rest API mapping has to be done here.
    // No application logic should be implemented in the class. Call the Service for all application logic.


   //TODO get students dto -> json
    //TODO get students dto - convert to entity - get courses - merge coursesdto
    //TODO get students dto get course dto merge into student. -> bedungungen beachten
    //TODO create Missing PrerequestisException

}

