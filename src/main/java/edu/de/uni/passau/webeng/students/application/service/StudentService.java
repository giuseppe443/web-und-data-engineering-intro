package edu.de.uni.passau.webeng.students.application.service;

import edu.de.uni.passau.webeng.students.application.exception.MissingPrerequisiteException;
import edu.de.uni.passau.webeng.students.model.Course;
import edu.de.uni.passau.webeng.students.model.Student;
import edu.de.uni.passau.webeng.students.web.dto.CourseDto;
import edu.de.uni.passau.webeng.students.web.dto.StudentDto;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.context.annotation.Bean;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ResponseStatus;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class StudentService {

    private static List<Student> students = new ArrayList<>();
    private static List<Course> courses = new ArrayList<>();

    static {
        // Initializes the data of our model
        Course course0 = new Course("c0", "JSF", "Learn JSF.", null);
        Course course1 = new Course("c1", "Maven", "One of the most popular dependency management tools.", null);
        Course course2 = new Course("c2", "Web Servers", "Learn about web servers.", null);

        List<Course> courseList0 = new LinkedList<>();
        courseList0.add(course1);
        courseList0.add(course2);
        Course course3 = new Course("c3", "Spring Boot", "Use Spring Boot to bootstrap servers.", courseList0);

        List<Course> courseList1 = new LinkedList<>();
        courseList1.addAll(courseList0);
        courseList1.add(course3);
        Course course4 = new Course("c4", "Spring Data", "A course about data persistence on the server.", courseList1);

        courses.add(course0);
        courses.add(course1);
        courses.add(course2);
        courses.add(course3);
        courses.add(course4);

        List<Course> courseList2 = new LinkedList<>();
        courseList2.add(course0);
        courseList2.add(course1);

        List<Course> courseList3 = new LinkedList<>();
        courseList3.add(course3);

        List<Course> courseList4 = new LinkedList<>();
        courseList4.add(course0);

        Student student0 = new Student("23328", "Max", "Muster", courseList2, null);
        Student student1 = new Student("34622", "Hans", "Muster", courseList3, courseList0);
        Student student2 = new Student("48645", "Alice", "Klint", courseList4, courseList0);
        Student student3 = new Student("24232", "Bob", "Ser", null, courseList1);

        students.add(student0);
        students.add(student1);
        students.add(student2);
        students.add(student3);
    }

    // TODO Add Service methods here
    //Noch nicht sicher warum der Mapper nicht geht?=!?!
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Autowired
    private ModelMapper modelMapper;

    public Student getStudentById(String id) {
        for (Student student : students) {
            if (student.getMatrNr().equals(id)) {
                return student;
            }
        }
        return null;
    }

    public StudentDto getStudentDtoById(String id) {
        for (Student student : students) {
            if (student.getMatrNr().equals(id)) {
                return convertToStudentDto(student);
            }
        }
        return null;
    }

    private Course getCourseById(String id) {
        for (Course course : courses) {
            if (course.getId().equals(id)) {
                return course;
            }
        }
        return null;
    }

    private StudentDto convertToStudentDto(Student student) {
        StudentDto studentDto = modelMapper.map(student, StudentDto.class);
        studentDto.setMatrNr(Long.parseLong(student.getMatrNr()));
        studentDto.setFirstName(student.getFirstName());
        studentDto.setLastName(student.getLastName());
        return studentDto;
    }

    public List<CourseDto> getCoursesOfStudent(Student student) {
        List<CourseDto> courseDtos = new ArrayList<>();
        if (student.getCourses() != null) {
            for (Course course : student.getCourses()) {
                courseDtos.add(convertToCourseDto(course));
            }

            return courseDtos;
        } else {
            throw new UnknownMatchException("Student has no courses");

        }
    }

    //TODO check mapper to parce c4 into 4 as argument for id.
    private CourseDto convertToCourseDto(Course course) {
        CourseDto courseDto = new CourseDto();
        String id = course.getId();
        id = id.replace("c", "");
        courseDto.setId(Long.parseLong(id));
        courseDto.setDescription(course.getDescription());
        courseDto.setTitle(course.getTitle());

        List<CourseDto> prerequisites = new ArrayList<>();
        if(course.getPrerequisite() != null){
            for (int i = 0; i < course.getPrerequisite().size(); i++) {
                CourseDto newCourse = convertToCourseDto(course.getPrerequisite().get(i));
                prerequisites.add(newCourse);
            }
        }
        courseDto.setPrerequisites(prerequisites);
        return courseDto;
    }

    public void addCourseToStudent(String id, String cid) {
        Student student = getStudentById(id);
        Course course = getCourseById(cid);
        //Check if course isn't already finished or currently visited
        if(student.getFinishedCourses() != null && (student.getFinishedCourses().contains(course) || student.getCourses().contains(course))){
            throw new UnknownMatchException("Course is already visited");
        } else {
            //Check if Prereq. is valid.
            if (course.getPrerequisite() != null) {
                if (student.getFinishedCourses() != null && student.getFinishedCourses().containsAll(course.getPrerequisite())) {
                    student.addCourses(course);
                } else {
                    String excep = "";
                    for (int i = 0; i < course.getPrerequisite().size(); i++) {
                        if (!(student.getFinishedCourses().contains(course.getPrerequisite().get(i)))) {
                            excep += course.getPrerequisite().get(i).getTitle();
                            excep += ", ";
                        }
                    }
                    throw new MissingPrerequisiteException(excep);
                }
            } else {
                student.addCourses(course);
            }
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public class UnknownMatchException extends RuntimeException {
        public UnknownMatchException(String matchId) {
            super("Unknown match: " + matchId);
        }
    }
}
