package edu.de.uni.passau.webeng.students.model;

import java.util.List;

public class Course {
	private String id;
	private String title;
	private String description;
	private List<Course> prerequisite;

	public Course(String id, String title, String description, List<Course> prerequisite) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.prerequisite = prerequisite;
	}

	public String getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public String getTitle() {
		return title;
	}
	public List<Course> getPrerequisite() {
		return prerequisite;
	}
}