package edu.de.uni.passau.webeng.students.model;

import java.util.List;

public class Student {
	private String mtrNumber;
	private String firstName;
	private String lastName;
	private List<Course> courses;
	private List<Course> finishedCourses;

	public Student(String mtrNumber, String firstName, String lastName, List<Course> courses, List<Course> finishedCourses) {
		this.mtrNumber = mtrNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.courses = courses;
		this.finishedCourses = finishedCourses;
	}

	 public void addCourses(Course course){
		courses.add(course);
	 }

	 public void addFinishedCoursesy(Course course){
	 	finishedCourses.add(course);
	 }

	 public String getMatrNr() {
		 return mtrNumber;
	 }

	public void setMatrNr(String matrNr) {
		this.mtrNumber = matrNr;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Course> getCourses() {
		return courses;
	}

	public List<Course> getFinishedCourses() { return finishedCourses;	}
}